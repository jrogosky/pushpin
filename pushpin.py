#=================================================
# NOTES
#=================================================
"""
TODO:
pinterest
instagram - only returning 13 results
via.me - no geolocation data in API
imgur - no search api
foursquare - won't let you search for geotagged data based on coords
bing - not gonna work. bad documentation.
twitpic - no search api
  http://icanstalku.com/how.php
timing filter by function or api?
  better to be function if results are stored to disk?
  search by time return better results?
tweetpaths.com
print json.dumps(jsonobj, indent=4)
images http://maps.google.com/mapfiles/ms/icons/$name"

DONE:
twitter
youtube
flickr
picasa
shodan
oodle
"""
#=================================================
# CLASS DECLARATIONS
#=================================================

import urllib2, json, time, os.path
from datetime import datetime

import lxml.etree as ET
import math

class pushpin():

    def __init__(self, lat=None, lon=None, rad=None):
        self.verbose = False
        self.debug = False
        self.lat = lat
        self.lon = lon
        self.rad = rad
        self.kismet_file = None
        self.db = {}
    #JR Added to set the kismet file location    
    def set_kismet(self,kismet_file):
        self.kismet_file = kismet_file

    def get_key(self, key_name):
        keyfile = 'api.keys'
        if os.path.exists(keyfile):
            for line in open(keyfile):
                key, value = line.split('::')[0], line.split('::')[1]
                if key == key_name:
                    return value.strip()
        try: key = raw_input("Enter API Key (blank to skip): ")
        except KeyboardInterrupt: return ''
        if key:
            file = open(keyfile, 'a')
            file.write('%s::%s\n' % (key_name, key))
            file.close()
        return key

    def save_db_to_file(self, filename):
        db = self.db
        db['meta'] = {}
        db['meta']['lat'] = self.lat
        db['meta']['lon'] = self.lon
        db['meta']['rad'] = self.rad
        file = open(filename, 'w')
        json.dump(db, file)
        file.close()
        del db['meta']

    def load_db_from_file(self, filename):
        db = json.loads(open(filename).read())
        self.lat = db['meta']['lat']
        self.lon = db['meta']['lon']
        self.rad = db['meta']['rad']
        del db['meta']
        self.db = db

    def format_time(self, stamp):
        return stamp.strftime('%Y-%m-%d %H:%M:%S')

    def time_filter(self, start, stop):
        for api in self.db.keys():
            filtered_data = []
            for entry in self.db[api]['api_data']:
                dt_entry = datetime.strptime(entry['media_time'], '%Y-%m-%d %H:%M:%S')
                if start <= dt_entry <= stop:
                    filtered_data.append(entry)
            self.db[api]['api_data'] = filtered_data

    def sanitize(self, htmlstring):
        # escape HTML with entities
        escapes = {'"': '&quot;',
                   "'": '&#39;',
                   '<': '&lt;',
                   '>': '&gt;'}
        #htmlstring = htmlstring.replace('&', '&amp;')
        for seq, esc in escapes.iteritems():
            htmlstring = htmlstring.replace(seq, esc)
        # ASCII encode the string and ignore errors
        #htmlstring = htmlstring.encode('ascii', 'ignore')
        # remove characters that aren't safe for JavaScript
        htmlstring = ''.join([char for char in htmlstring if ord(char) >= 32 and ord(char) <= 126])
        return htmlstring

    def key(self, item):
        return item['media_time']

    def create_media_content(self):
        content = ''
        summary = ':'
        for api in self.db.keys():
            if not self.db[api]['api_data']: continue
            # build media content
            content += '<div class="media_column %s">\n<table>\n<tr><td colspan="2" class="header_cell"><img src="images/%s" /><br /><hr width="95%%" /></td></tr>\n' % (self.db[api]['media_style_class'], self.db[api]['media_header_img'])
            for item in sorted(self.db[api]['api_data'], key=self.key, reverse=True):
                content += '<tr><td class="prof_cell"><a href="%s" target="_blank"><img class="prof_img" src="%s" /></a></td><td class="data_cell"><div class="trigger" id="trigger" lat="%s" lon="%s">[<a href="%s" target="_blank">%s</a>] %s<br /><span class="time">%s</span></div></td></tr>\n' % (item['media_url'], item['thumb_url'], item['lat'], item['lon'], item['profile_url'], item['display_name'], item['text'], item['media_time'])
            content += '</table>\n</div>\n'
            # build summary content
            summary += ': %d %s :' % (len(self.db[api]['api_data']), api)
        summary += ':'
        return content, summary

    def create_map_content(self):
        content = ''
        for api in self.db.keys():
            for item in self.db[api]['api_data']:
                details = "<table><tr><td class='prof_cell'><a href='%s' target='_blank'><img class='prof_img' src='%s' /></a></td class='data_cell'><td>[<a href='%s' target='_blank'>%s</a>] %s<br /><span class='time'>%s</span></td></tr></table>" % (item['media_url'], item['thumb_url'], item['profile_url'], item['display_name'], item['text'], item['media_time'])
                content += '\t\tadd_marker({position: new google.maps.LatLng(%s,%s),title:"%s",icon:"%s",map:map},{details:"%s"});\n' % (item['lat'], item['lon'], item['display_name'], self.db[api]['map_pin_img'], details)
        return content

    def write_markup(self, template, filename, content, summary=''):
        map_content = open(template).read()
        page = map_content % (self.lat, self.lon, self.rad, summary, content)
        file = open('site/%s' % filename, 'w')
        file.write(page)#.encode('utf-8'))
        file.close()

    def api_to_db(self, api_name, func_name, media_style_class='', media_header_img='', map_pin_img='http://maps.google.com/mapfiles/ms/icons/red-dot.png'):
        self.db[api_name] = {}
        self.db[api_name]['media_style_class'] = media_style_class
        self.db[api_name]['media_header_img'] = media_header_img
        self.db[api_name]['map_pin_img'] = map_pin_img
        print '[-] Getting Data from %s...' % api_name
        func = getattr(self, func_name)
        self.db[api_name]['api_data'] = func()

    def get_instagram(self):
        api_data = []
        api_key = self.get_key('instagram')
        if not api_key:
            print '[!] No API Key. Skipping Module.'
            return api_data
        base_url = 'https://api.instagram.com/v1/media/search'
        params = 'client_id=%s&lat=%s&lng=%s&distance=%d&count=20' % (api_key, self.lat, self.lon, (float(self.rad) * 1000))
        url = '%s?%s' % (base_url, params)
        while True:
            if self.debug: print '[!] ' % (url)
            try: response = urllib2.urlopen(url)
            except urllib2.HTTPError as e:
                if self.debug: print '[!] Error: %s in %s' % (e, url)
                continue
            jsonstr = response.read()
            if not jsonstr: break
            try: jsonobj = json.loads(jsonstr)
            except ValueError as e:
                if self.debug: print '[!] Error: %s in %s' % (e, url)
                continue
            if not len(jsonobj['data']) > 0: break
            for item in jsonobj['data']:
                dict = {}
                try:
                    dict['source'] = 'Instagram'
                    dict['media_url'] = item['images']['standard_resolution']['url']
                    dict['thumb_url'] = item['images']['thumbnail']['url']
                    dict['lat'] = item['location']['latitude']
                    dict['lon'] = item['location']['longitude']
                    dict['profile_url'] = 'http://instagram.com/'
                    dict['display_name'] = self.sanitize(item['user']['full_name']) #['username']
                    dict['text'] = self.sanitize(item['caption']['text'])
                    dict['media_time'] = self.format_time(datetime.fromtimestamp(float(item['created_time'])))
                except Exception as e:
                    if self.debug: print '[!] Error: %s in %s' % (e, item)
                    continue
                api_data.append(dict)
                if self.verbose: print '[+] %s - %s' % (dict['display_name'], dict['text'])
            #if jsonobj['photos']['page'] == jsonobj['photos']['pages']: break
            #if self.verbose: print '[-] Page: %d' % (jsonobj['photos']['page'] + 1)
            #url = '%s?%s&page=%s' % (base_url, params, str(jsonobj['photos']['page'] + 1))
            break
        return api_data

    def get_oodle(self):
        api_data = []
        api_key = self.get_key('oodle')
        if not api_key:
            print '[!] No API Key. Skipping Module.'
            return api_data
        base_url = 'http://api.oodle.com/api/v2/listings'
        categories = ['community', 'community/announcements', 'community/announcements/auction', 'community/announcements/celebration', 'community/announcements/celebration/anniversary', 'community/announcements/celebration/birthdays', 'community/announcements/celebration/births', 'community/announcements/celebration/congrats', 'community/announcements/celebration/engagements', 'community/announcements/celebration/marriages', 'community/announcements/celebration/other', 'community/announcements/celebration/seasonal', 'community/announcements/celebration/thanks', 'community/announcements/company', 'community/announcements/company/business_opportunities', 'community/announcements/death', 'community/announcements/death/funerals', 'community/announcements/government', 'community/announcements/legal', 'community/announcements/lost', 'community/announcements/other', 'community/announcements/proposals', 'community/announcements/trustee', 'community/groups', 'community/news', 'community/news/film', 'community/news/music', 'community/news/other', 'community/news/visual_art', 'community/politics', 'community/rants', 'community/rideshare', 'community/wanted', 'housing', 'housing/rent', 'housing/rent/apartment', 'housing/rent/commercial', 'housing/rent/commercial/industrial', 'housing/rent/commercial/office_space', 'housing/rent/commercial/retail', 'housing/rent/condo', 'housing/rent/garage', 'housing/rent/home', 'housing/rent/mobile', 'housing/rent/open_house', 'housing/rent/other', 'housing/rent/roommates', 'housing/rent/short_term', 'housing/rent/storage', 'housing/rent/vacation', 'housing/sale', 'housing/sale/commercial', 'housing/sale/commercial/industrial', 'housing/sale/commercial/office_space', 'housing/sale/commercial/retail', 'housing/sale/condo', 'housing/sale/farm', 'housing/sale/foreclosure', 'housing/sale/home', 'housing/sale/land', 'housing/sale/mobile', 'housing/sale/multi_family', 'housing/sale/open_house', 'housing/sale/other', 'housing/sale/storage', 'housing/sale/vacation', 'job', 'job/admin', 'job/advertising', 'job/architecture', 'job/art', 'job/art/other', 'job/art/visual_art', 'job/auto', 'job/bank', 'job/biotech', 'job/business_opportunities', 'job/civil_service', 'job/computer', 'job/construction', 'job/consulting', 'job/customer_service', 'job/distribution', 'job/domestic_help', 'job/education', 'job/energy', 'job/engineer', 'job/entertainment', 'job/entertainment/actor', 'job/entertainment/actor/adult', 'job/entertainment/dancer', 'job/entertainment/dancer/adult', 'job/entertainment/model', 'job/entertainment/model/adult', 'job/entertainment/music', 'job/entertainment/post_production', 'job/entertainment/production', 'job/facilities', 'job/finance', 'job/fitness', 'job/franchise', 'job/general_business', 'job/general_labor', 'job/grocery', 'job/health', 'job/hospitality', 'job/hr', 'job/insurance', 'job/inventory', 'job/law_enforcement', 'job/legal', 'job/logistics', 'job/management', 'job/military', 'job/nonprofit', 'job/other', 'job/paralegal', 'job/production_operation', 'job/professional_services', 'job/qa', 'job/rd', 'job/real_estate', 'job/repair', 'job/restaurant', 'job/retail', 'job/sales', 'job/salon', 'job/science', 'job/social_services', 'job/strategy_planning', 'job/tech', 'job/telecom', 'job/training', 'job/vet', 'job/warehouse', 'job/work_from_home', 'job/writing', 'personals', 'personals/men_seeking_both', 'personals/men_seeking_men', 'personals/men_seeking_women', 'personals/missed_connections', 'personals/other', 'personals/women_seeking_both', 'personals/women_seeking_men', 'personals/women_seeking_women', 'sale', 'sale/adult', 'sale/antiques', 'sale/appliance', 'sale/appliance/dishwasher', 'sale/appliance/microwave', 'sale/appliance/other', 'sale/appliance/oven', 'sale/appliance/refrigerator', 'sale/appliance/refrigerator/freezer', 'sale/appliance/refrigerator/refrigerator', 'sale/appliance/small', 'sale/appliance/small/blender', 'sale/appliance/small/bread_machine', 'sale/appliance/small/coffee_maker', 'sale/appliance/small/food_processor', 'sale/appliance/small/juicer', 'sale/appliance/small/mixer', 'sale/appliance/small/other', 'sale/appliance/small/toaster', 'sale/appliance/small/water_filter', 'sale/appliance/washer', 'sale/appliance/washer/dryer', 'sale/appliance/washer/washer', 'sale/appliance/water_heater', 'sale/art', 'sale/art/art', 'sale/art/craft', 'sale/bag', 'sale/bag/backpack', 'sale/bag/briefcase', 'sale/bag/handbag', 'sale/bag/luggage', 'sale/bag/wallet', 'sale/book', 'sale/book/audio', 'sale/book/books', 'sale/book/kids', 'sale/book/magazine', 'sale/book/manual', 'sale/book/textbook', 'sale/business', 'sale/business/building', 'sale/business/farm', 'sale/business/medical', 'sale/business/office', 'sale/business/retail', 'sale/business/sale', 'sale/cd_dvd', 'sale/cd_dvd/blu-ray', 'sale/cd_dvd/cd', 'sale/cd_dvd/dvd', 'sale/cd_dvd/other', 'sale/cd_dvd/vhs', 'sale/cellphone', 'sale/cellphone/android', 'sale/cellphone/blackberry', 'sale/cellphone/iphone', 'sale/cellphone/other', 'sale/cellphone/windows', 'sale/clothes', 'sale/clothes/accessories', 'sale/clothes/accessories/belt', 'sale/clothes/accessories/glove', 'sale/clothes/accessories/hair', 'sale/clothes/accessories/hats', 'sale/clothes/accessories/neckware', 'sale/clothes/accessories/sunglasses', 'sale/clothes/coats', 'sale/clothes/dresses', 'sale/clothes/exercise', 'sale/clothes/military', 'sale/clothes/other', 'sale/clothes/outfits', 'sale/clothes/pants', 'sale/clothes/shirts', 'sale/clothes/shoes', 'sale/clothes/shorts', 'sale/clothes/skirts', 'sale/clothes/sleepwear', 'sale/clothes/socks', 'sale/clothes/suits', 'sale/clothes/sweater', 'sale/clothes/swimming', 'sale/clothes/wedding', 'sale/collectible', 'sale/collectible/cards', 'sale/collectible/coins', 'sale/collectible/comics', 'sale/collectible/military', 'sale/collectible/sports', 'sale/collectible/stamps', 'sale/computer', 'sale/computer/component', 'sale/computer/desktop', 'sale/computer/input', 'sale/computer/input/keyboard', 'sale/computer/input/mouse', 'sale/computer/laptop', 'sale/computer/monitor', 'sale/computer/monitor/crt', 'sale/computer/monitor/lcd', 'sale/computer/network', 'sale/computer/network/hub', 'sale/computer/network/load_balancer', 'sale/computer/network/other', 'sale/computer/network/router', 'sale/computer/network/switch', 'sale/computer/network/wifi', 'sale/computer/other', 'sale/computer/printer', 'sale/computer/printer/all', 'sale/computer/printer/inkjet', 'sale/computer/printer/laser', 'sale/computer/printer/scanner', 'sale/computer/printer/supply', 'sale/computer/server', 'sale/computer/software', 'sale/computer/storage', 'sale/computer/storage/cd', 'sale/computer/storage/dvd', 'sale/computer/storage/external', 'sale/computer/storage/flash', 'sale/computer/storage/floppy', 'sale/computer/storage/internal', 'sale/computer/storage/media', 'sale/electronics', 'sale/electronics/audio', 'sale/electronics/audio/home', 'sale/electronics/audio/home/accessories', 'sale/electronics/audio/home/amplifier', 'sale/electronics/audio/home/cassette_deck', 'sale/electronics/audio/home/cd_player', 'sale/electronics/audio/home/equalizer', 'sale/electronics/audio/home/media', 'sale/electronics/audio/home/receiver', 'sale/electronics/audio/home/speaker', 'sale/electronics/audio/home/system', 'sale/electronics/audio/home/turntable', 'sale/electronics/audio/personal', 'sale/electronics/audio/personal/boombox', 'sale/electronics/audio/personal/cassette_player', 'sale/electronics/audio/personal/cd_player', 'sale/electronics/audio/personal/clock_radio', 'sale/electronics/audio/personal/headphone', 'sale/electronics/camera', 'sale/electronics/camera/accessories', 'sale/electronics/camera/digital', 'sale/electronics/camera/film', 'sale/electronics/camera/other', 'sale/electronics/car', 'sale/electronics/car/gps', 'sale/electronics/car/radar', 'sale/electronics/car/stereo', 'sale/electronics/dvd', 'sale/electronics/dvr', 'sale/electronics/gps', 'sale/electronics/ipod', 'sale/electronics/ipod/ipod', 'sale/electronics/ipod/other', 'sale/electronics/other', 'sale/electronics/phone', 'sale/electronics/satellite', 'sale/electronics/tablets', 'sale/electronics/tablets/android', 'sale/electronics/tablets/ipad', 'sale/electronics/tablets/other', 'sale/electronics/tv', 'sale/electronics/vcr', 'sale/electronics/video', 'sale/free', 'sale/furniture', 'sale/furniture/bed', 'sale/furniture/bed/bunk', 'sale/furniture/bed/set', 'sale/furniture/bed/water', 'sale/furniture/bench', 'sale/furniture/bench/bench', 'sale/furniture/bench/stool', 'sale/furniture/bookcase', 'sale/furniture/cabinet', 'sale/furniture/cabinet/armoire', 'sale/furniture/cabinet/cabinet', 'sale/furniture/cabinet/cupboard', 'sale/furniture/chair', 'sale/furniture/chair/recliner', 'sale/furniture/chair/rocking', 'sale/furniture/desk', 'sale/furniture/dresser', 'sale/furniture/other', 'sale/furniture/other/clock', 'sale/furniture/other/credenza', 'sale/furniture/other/lamp', 'sale/furniture/other/mirror', 'sale/furniture/rug', 'sale/furniture/sofa', 'sale/furniture/sofa/couch', 'sale/furniture/sofa/futon', 'sale/furniture/sofa/loveseat', 'sale/furniture/sofa/sectional', 'sale/furniture/sofa/sofabed', 'sale/furniture/table', 'sale/furniture/table/changing', 'sale/furniture/table/coffee', 'sale/furniture/table/end', 'sale/furniture/table/stand', 'sale/health', 'sale/health/cosmetics', 'sale/health/hair', 'sale/health/skin_care', 'sale/health/supplements', 'sale/home', 'sale/home/bedding', 'sale/home/clean', 'sale/home/decor', 'sale/home/floor', 'sale/home/food', 'sale/home/heat', 'sale/home/heat/ac', 'sale/home/heat/firewood', 'sale/home/heat/heat', 'sale/home/kitchen', 'sale/home/lawn', 'sale/home/lawnmower', 'sale/home/light', 'sale/home/pool', 'sale/home/security', 'sale/home/windows', 'sale/jewelry', 'sale/jewelry/bracelet', 'sale/jewelry/earring', 'sale/jewelry/necklace', 'sale/jewelry/ring', 'sale/jewelry/watch', 'sale/kid', 'sale/kid/accessories', 'sale/kid/backpack', 'sale/kid/bedding', 'sale/kid/bikes', 'sale/kid/books', 'sale/kid/car_seat', 'sale/kid/clothes', 'sale/kid/crib', 'sale/kid/furniture', 'sale/kid/safety', 'sale/kid/stroller', 'sale/kid/toy', 'sale/kid/toy/baby', 'sale/kid/toy/car', 'sale/kid/toy/dolls', 'sale/kid/toy/educational', 'sale/kid/toy/games', 'sale/kid/toy/outdoor', 'sale/kid/toy/stuffed', 'sale/music', 'sale/music/accessories', 'sale/music/accessories/metronome', 'sale/music/accessories/musicstand', 'sale/music/accessories/sheetmusic', 'sale/music/accessories/tune', 'sale/music/brass', 'sale/music/brass/baritone', 'sale/music/brass/bugle', 'sale/music/brass/cornet', 'sale/music/brass/flugelhorn', 'sale/music/brass/french_horn', 'sale/music/brass/trombone', 'sale/music/brass/trumpet', 'sale/music/brass/tuba', 'sale/music/drum', 'sale/music/drum/base', 'sale/music/drum/bongo', 'sale/music/drum/chimes', 'sale/music/drum/conga', 'sale/music/drum/cowbell', 'sale/music/drum/cymbal', 'sale/music/drum/drum', 'sale/music/drum/glockespiel', 'sale/music/drum/gong', 'sale/music/drum/hi_hat', 'sale/music/drum/maracas', 'sale/music/drum/percussion', 'sale/music/drum/snare', 'sale/music/drum/tambourine', 'sale/music/drum/timpani', 'sale/music/drum/tomtom', 'sale/music/drum/triangle', 'sale/music/drum/xylophone', 'sale/music/guitar', 'sale/music/guitar/banjo', 'sale/music/guitar/bass', 'sale/music/guitar/dulcimer', 'sale/music/guitar/guitar', 'sale/music/guitar/lute', 'sale/music/guitar/mandolin', 'sale/music/guitar/ukulele', 'sale/music/other', 'sale/music/piano', 'sale/music/piano/accordian', 'sale/music/piano/keyboard', 'sale/music/piano/organ', 'sale/music/piano/piano', 'sale/music/piano/synthesizer', 'sale/music/string', 'sale/music/string/autoharp', 'sale/music/string/cello', 'sale/music/string/harp', 'sale/music/string/viola', 'sale/music/string/violin', 'sale/music/wind', 'sale/music/wind/bassoon', 'sale/music/wind/clarinet', 'sale/music/wind/flute', 'sale/music/wind/harmonica', 'sale/music/wind/oboe', 'sale/music/wind/piccolo', 'sale/music/wind/recorder', 'sale/music/wind/saxophone', 'sale/other', 'sale/pet', 'sale/pet/bird', 'sale/pet/cat', 'sale/pet/dog', 'sale/pet/fish', 'sale/pet/horse', 'sale/pet/livestock', 'sale/pet/livestock/supply', 'sale/pet/other', 'sale/pet/rabbit', 'sale/pet/reptile', 'sale/pet/small_furry', 'sale/pet/supply', 'sale/pet/supply/bird', 'sale/pet/supply/cat', 'sale/pet/supply/dog', 'sale/pet/supply/fish', 'sale/pet/supply/horse', 'sale/sale', 'sale/sale/auction', 'sale/sale/estate', 'sale/sale/garage', 'sale/sport', 'sale/sport/bike', 'sale/sport/bike/apparel', 'sale/sport/bike/helmet', 'sale/sport/bike/kid', 'sale/sport/bike/mountain', 'sale/sport/bike/parts', 'sale/sport/bike/road', 'sale/sport/camping', 'sale/sport/equestrian', 'sale/sport/exercise', 'sale/sport/exercise/treadmill', 'sale/sport/exercise/treadmill/stair', 'sale/sport/exercise/treadmill/treadmill', 'sale/sport/hunting', 'sale/sport/hunting/fishing', 'sale/sport/hunting/hunting', 'sale/sport/indoor', 'sale/sport/indoor/air_hockey', 'sale/sport/indoor/darts', 'sale/sport/indoor/foosball', 'sale/sport/indoor/ping_pong', 'sale/sport/indoor/pool_table', 'sale/sport/other', 'sale/sport/snow', 'sale/sport/snow/ski', 'sale/sport/snow/snowboard', 'sale/sport/sport', 'sale/sport/sport/baseball', 'sale/sport/sport/basketball', 'sale/sport/sport/football', 'sale/sport/sport/golf', 'sale/sport/sport/hockey', 'sale/sport/sport/other', 'sale/sport/sport/soccer', 'sale/sport/sport/tennis', 'sale/sport/water', 'sale/sport/water/dive', 'sale/sport/water/other', 'sale/sport/water/paddle', 'sale/sport/water/paddle/canoe', 'sale/sport/water/paddle/kayak', 'sale/sport/water/paddle/rafts', 'sale/sport/water/ski', 'sale/sport/water/surf', 'sale/sport/water/wind', 'sale/sport/weight', 'sale/tickets', 'sale/tickets/concert', 'sale/tickets/group', 'sale/tickets/group/business_networking', 'sale/tickets/group/class_workshop', 'sale/tickets/group/conference_seminar', 'sale/tickets/group/dance', 'sale/tickets/group/festival', 'sale/tickets/group/food_wine', 'sale/tickets/group/free', 'sale/tickets/group/health_spiritual', 'sale/tickets/group/kids_family', 'sale/tickets/group/other', 'sale/tickets/group/sports_outdoors', 'sale/tickets/other', 'sale/tickets/other/movie', 'sale/tickets/other/travel', 'sale/tickets/sports', 'sale/tickets/sports/auto_racing', 'sale/tickets/sports/baseball', 'sale/tickets/sports/basketball', 'sale/tickets/sports/boxing', 'sale/tickets/sports/cricket', 'sale/tickets/sports/football', 'sale/tickets/sports/golf', 'sale/tickets/sports/hockey', 'sale/tickets/sports/horse_racing', 'sale/tickets/sports/olympics', 'sale/tickets/sports/other', 'sale/tickets/sports/rugby', 'sale/tickets/sports/soccer', 'sale/tickets/sports/tennis', 'sale/tickets/sports/track_field', 'sale/tickets/sports/wrestling', 'sale/tickets/theater', 'sale/tool', 'sale/tool/hand', 'sale/tool/hand/axe', 'sale/tool/hand/chisel', 'sale/tool/hand/clamps_vises', 'sale/tool/hand/hammer', 'sale/tool/hand/knife', 'sale/tool/hand/ladder', 'sale/tool/hand/level', 'sale/tool/hand/measure', 'sale/tool/hand/other', 'sale/tool/hand/plane', 'sale/tool/hand/plier', 'sale/tool/hand/pry_bar', 'sale/tool/hand/puller', 'sale/tool/hand/punch', 'sale/tool/hand/saw', 'sale/tool/hand/saw_horse', 'sale/tool/hand/screwdriver', 'sale/tool/hand/sledge_hammer', 'sale/tool/hand/socket', 'sale/tool/hand/square', 'sale/tool/hand/stud_sensor', 'sale/tool/hand/tool_set', 'sale/tool/hand/wrench', 'sale/tool/other', 'sale/tool/power', 'sale/tool/power/air_compress', 'sale/tool/power/air_hammer', 'sale/tool/power/air_ratchet', 'sale/tool/power/chipper', 'sale/tool/power/drill', 'sale/tool/power/engraver', 'sale/tool/power/generator', 'sale/tool/power/grinder', 'sale/tool/power/impact_wrench', 'sale/tool/power/lathe', 'sale/tool/power/leaf_blower', 'sale/tool/power/nail_gun', 'sale/tool/power/planer', 'sale/tool/power/polisher', 'sale/tool/power/pressure_washer', 'sale/tool/power/router', 'sale/tool/power/sander', 'sale/tool/power/saw', 'sale/tool/power/saw/band', 'sale/tool/power/saw/chain', 'sale/tool/power/saw/circular', 'sale/tool/power/saw/miter', 'sale/tool/power/saw/reciprocating', 'sale/tool/power/saw/scroll', 'sale/tool/power/saw/table', 'sale/tool/power/saw/tile', 'sale/tool/power/welding_soldering', 'sale/tool/wood', 'sale/video_game', 'sale/video_game/console', 'sale/video_game/game', 'sale/video_game/portable', 'service', 'service/adult', 'service/adult/chat', 'service/adult/friend', 'service/adult/other', 'service/adult/psychic', 'service/adult/relax', 'service/adult/work', 'service/car', 'service/car/detailing', 'service/car/oil_change', 'service/car/rental', 'service/car/repair', 'service/car/tires', 'service/care', 'service/care/child', 'service/care/child/babysitter', 'service/care/child_safety', 'service/care/elderly', 'service/care/nurse', 'service/cleaning', 'service/cleaning/carpet', 'service/cleaning/chimney', 'service/cleaning/exterior', 'service/cleaning/interior', 'service/cleaning/junk', 'service/cleaning/window', 'service/coupon', 'service/coupon/art', 'service/coupon/book', 'service/coupon/car', 'service/coupon/clothes', 'service/coupon/dining', 'service/coupon/education', 'service/coupon/electronics', 'service/coupon/entertainment', 'service/coupon/food', 'service/coupon/gift', 'service/coupon/health', 'service/coupon/home', 'service/coupon/kid', 'service/coupon/office', 'service/coupon/other', 'service/coupon/pet', 'service/coupon/phone', 'service/coupon/service', 'service/coupon/sport', 'service/coupon/travel', 'service/creative', 'service/creative/design', 'service/creative/film', 'service/creative/music', 'service/creative/other', 'service/creative/photo', 'service/education', 'service/education/art', 'service/education/cooking', 'service/education/dance', 'service/education/driving', 'service/education/language', 'service/education/music', 'service/education/other', 'service/education/school', 'service/education/sports', 'service/education/sports/golf', 'service/education/tech', 'service/education/tutor', 'service/entertainment', 'service/entertainment/catering', 'service/entertainment/event', 'service/entertainment/kid', 'service/entertainment/music', 'service/entertainment/other', 'service/entertainment/rental', 'service/finance', 'service/finance/accountant', 'service/finance/credit', 'service/finance/financial', 'service/finance/insurance', 'service/finance/investments', 'service/finance/loans', 'service/finance/loans/auto', 'service/finance/loans/home', 'service/finance/other', 'service/finance/tax', 'service/food', 'service/food/bar', 'service/food/restaurant', 'service/health', 'service/health/alternative', 'service/health/art', 'service/health/chiropractor', 'service/health/counseling', 'service/health/fitness', 'service/health/hair', 'service/health/massage', 'service/health/medical', 'service/health/other', 'service/health/pedicure', 'service/health/skin_care', 'service/home', 'service/home/appliance', 'service/home/appliance/install', 'service/home/appliance/repair', 'service/home/construction', 'service/home/construction/addition', 'service/home/construction/demolition', 'service/home/construction/new', 'service/home/construction/professional', 'service/home/construction/professional/appraiser', 'service/home/construction/professional/architect', 'service/home/construction/professional/engineer', 'service/home/construction/professional/other', 'service/home/construction/professional/survey', 'service/home/construction/remodel', 'service/home/construction/sheds', 'service/home/decor', 'service/home/door', 'service/home/door/cover', 'service/home/door/door', 'service/home/door/garage', 'service/home/door/locks_screens', 'service/home/door/window', 'service/home/door/window/tinting', 'service/home/door/window/treatment', 'service/home/electrical', 'service/home/electrical/electrical', 'service/home/electrical/generator', 'service/home/electrical/lighting', 'service/home/electrical/lighting/holiday', 'service/home/electrical/security', 'service/home/floor', 'service/home/floor/carpet', 'service/home/floor/concrete', 'service/home/floor/rock', 'service/home/floor/sport', 'service/home/floor/tile', 'service/home/floor/vinyl', 'service/home/floor/wood', 'service/home/furniture', 'service/home/furniture/repair', 'service/home/furniture/upholstry', 'service/home/handyman', 'service/home/heat', 'service/home/heat/ac', 'service/home/heat/central', 'service/home/heat/ducts', 'service/home/heat/fan', 'service/home/heat/fireplace', 'service/home/heat/fuel', 'service/home/heat/furnace', 'service/home/heat/hvac', 'service/home/heat/radiant', 'service/home/heat/thermostat', 'service/home/heat/water', 'service/home/other', 'service/home/other/disable', 'service/home/other/green', 'service/home/other/metal', 'service/home/other/pest_control', 'service/home/other/recovery', 'service/home/other/rental', 'service/home/other/security', 'service/home/paint', 'service/home/paint/exterior', 'service/home/paint/interior', 'service/home/paint/removal', 'service/home/paint/special', 'service/home/plumbing', 'service/home/plumbing/drain', 'service/home/plumbing/fire_sprinkler', 'service/home/plumbing/fixtures', 'service/home/plumbing/other', 'service/home/plumbing/pump', 'service/home/plumbing/septic', 'service/home/plumbing/water_softener', 'service/home/rock', 'service/home/rock/driveway', 'service/home/rock/fireplace', 'service/home/rock/floor', 'service/home/rock/other', 'service/home/roof', 'service/home/roof/cleaning', 'service/home/roof/gutter', 'service/home/roof/new', 'service/home/roof/repair', 'service/home/roof/siding', 'service/home/roof/skylight', 'service/home/wall', 'service/home/wall/ceiling', 'service/home/wall/drywall', 'service/home/wall/insulation', 'service/home/wall/wallpaper', 'service/home/wood', 'service/home/wood/cabinet', 'service/home/wood/closet', 'service/home/wood/countertop', 'service/home/wood/finish', 'service/home/wood/frame', 'service/home/wood/stair', 'service/job', 'service/lawn', 'service/lawn/blacktop', 'service/lawn/deck', 'service/lawn/fence', 'service/lawn/fountain', 'service/lawn/gardener', 'service/lawn/landscape', 'service/lawn/landscape/design', 'service/lawn/landscape/sprinkler', 'service/lawn/other', 'service/lawn/rock', 'service/lawn/rock/patio', 'service/lawn/rock/wall', 'service/lawn/snow_removal', 'service/lawn/tree', 'service/legal', 'service/move', 'service/move/hauler', 'service/move/mover', 'service/move/storage', 'service/other', 'service/other/special', 'service/pet', 'service/pet/grooming', 'service/pet/kennel', 'service/pet/other', 'service/pet/sitter', 'service/pet/veterinarian', 'service/pet/walker', 'service/pool', 'service/pool/pool', 'service/pool/pool/maintenance', 'service/pool/pool/new', 'service/pool/pool/repair', 'service/pool/sauna', 'service/pool/sauna/maintenance', 'service/pool/sauna/new', 'service/pool/sauna/repair', 'service/pool/tub', 'service/pool/tub/maintenance', 'service/pool/tub/new', 'service/pool/tub/repair', 'service/psychic', 'service/real_estate', 'service/real_estate/agent', 'service/real_estate/appraiser', 'service/real_estate/inspector', 'service/real_estate/inspector/pest', 'service/real_estate/inspector/roof', 'service/real_estate/inspector/water', 'service/real_estate/other', 'service/real_estate/staging', 'service/tech', 'service/tech/audio_video', 'service/tech/computer', 'service/tech/home_network', 'service/tech/repair', 'service/transportation', 'service/transportation/transportation', 'service/transportation/travel', 'vehicle', 'vehicle/airplane', 'vehicle/atv', 'vehicle/boat', 'vehicle/boat/commercial', 'vehicle/boat/fishing', 'vehicle/boat/fishing/fresh', 'vehicle/boat/fishing/salt', 'vehicle/boat/high-performance', 'vehicle/boat/house', 'vehicle/boat/inflatable', 'vehicle/boat/jetski', 'vehicle/boat/motor', 'vehicle/boat/pontoon', 'vehicle/boat/sailboat', 'vehicle/boat/small', 'vehicle/boat/trailer', 'vehicle/boat/wakeboard', 'vehicle/boat/yacht', 'vehicle/car', 'vehicle/car/classic', 'vehicle/car/convertible', 'vehicle/car/coupe', 'vehicle/car/hatchback', 'vehicle/car/hybrid', 'vehicle/car/mini_van', 'vehicle/car/sedan', 'vehicle/car/station_wagon', 'vehicle/car/suv', 'vehicle/car/truck', 'vehicle/car/van', 'vehicle/commercial_truck', 'vehicle/commercial_truck/auto_carrier', 'vehicle/commercial_truck/bucket', 'vehicle/commercial_truck/cab_and_chassis', 'vehicle/commercial_truck/cabover', 'vehicle/commercial_truck/cabover/sleeper', 'vehicle/commercial_truck/construction_vocational', 'vehicle/commercial_truck/conventional_sleeper', 'vehicle/commercial_truck/crane', 'vehicle/commercial_truck/day_cab', 'vehicle/commercial_truck/dump', 'vehicle/commercial_truck/expeditor_hot_shot', 'vehicle/commercial_truck/flatbed', 'vehicle/commercial_truck/grain', 'vehicle/commercial_truck/logging', 'vehicle/commercial_truck/mixers', 'vehicle/commercial_truck/other', 'vehicle/commercial_truck/refridgerated', 'vehicle/commercial_truck/refuse', 'vehicle/commercial_truck/refuse/front_loading', 'vehicle/commercial_truck/refuse/rear_loading', 'vehicle/commercial_truck/refuse/recycling', 'vehicle/commercial_truck/rollback', 'vehicle/commercial_truck/rolloff', 'vehicle/commercial_truck/service_utility', 'vehicle/commercial_truck/severe_duty', 'vehicle/commercial_truck/tank', 'vehicle/commercial_truck/tow', 'vehicle/commercial_truck/trailer', 'vehicle/commercial_truck/trailer/beam', 'vehicle/commercial_truck/trailer/belt', 'vehicle/commercial_truck/trailer/car_carrier', 'vehicle/commercial_truck/trailer/chassis', 'vehicle/commercial_truck/trailer/container', 'vehicle/commercial_truck/trailer/converter_dollies', 'vehicle/commercial_truck/trailer/curtainside', 'vehicle/commercial_truck/trailer/drop_deck', 'vehicle/commercial_truck/trailer/drop_deck/double', 'vehicle/commercial_truck/trailer/drop_deck/expandable', 'vehicle/commercial_truck/trailer/drop_deck/expandable_double', 'vehicle/commercial_truck/trailer/dump', 'vehicle/commercial_truck/trailer/equipment', 'vehicle/commercial_truck/trailer/flatbed', 'vehicle/commercial_truck/trailer/flatbed/expandable', 'vehicle/commercial_truck/trailer/grain', 'vehicle/commercial_truck/trailer/hopper', 'vehicle/commercial_truck/trailer/livestock', 'vehicle/commercial_truck/trailer/log', 'vehicle/commercial_truck/trailer/lowboy', 'vehicle/commercial_truck/trailer/open_top', 'vehicle/commercial_truck/trailer/other', 'vehicle/commercial_truck/trailer/platform', 'vehicle/commercial_truck/trailer/reefer', 'vehicle/commercial_truck/trailer/rolloff', 'vehicle/commercial_truck/trailer/specialty', 'vehicle/commercial_truck/trailer/storage', 'vehicle/commercial_truck/trailer/tag', 'vehicle/commercial_truck/trailer/tank', 'vehicle/commercial_truck/trailer/van', 'vehicle/commercial_truck/van', 'vehicle/commercial_truck/yard', 'vehicle/heavy_equipment', 'vehicle/motorcycle', 'vehicle/motorcycle/classic', 'vehicle/motorcycle/competition', 'vehicle/motorcycle/custom', 'vehicle/motorcycle/electric', 'vehicle/motorcycle/golfcart', 'vehicle/motorcycle/mini', 'vehicle/motorcycle/offroad', 'vehicle/motorcycle/offroad/dirt-bike', 'vehicle/motorcycle/offroad/mx', 'vehicle/motorcycle/offroad/super-moto', 'vehicle/motorcycle/road', 'vehicle/motorcycle/road/cruiser', 'vehicle/motorcycle/road/dualsport', 'vehicle/motorcycle/road/sport-touring', 'vehicle/motorcycle/road/sportbike', 'vehicle/motorcycle/road/standard', 'vehicle/motorcycle/road/touring', 'vehicle/motorcycle/scooter', 'vehicle/motorcycle/trailer', 'vehicle/motorcycle/trike', 'vehicle/other', 'vehicle/parts', 'vehicle/parts/airplane', 'vehicle/parts/atv', 'vehicle/parts/boat', 'vehicle/parts/car', 'vehicle/parts/motorcycle', 'vehicle/parts/rv', 'vehicle/parts/snowmobile', 'vehicle/powersports', 'vehicle/rv', 'vehicle/rv/camper', 'vehicle/rv/motorhome', 'vehicle/rv/trailer', 'vehicle/snowmobile', 'vehicle/storage', 'vehicle/storage/airplane', 'vehicle/storage/boat', 'vehicle/storage/rv']
        cat_cnt = 1
        cat_tot = len(categories)
        for category in categories:
            if self.verbose: print '[*] Searching Category [%d/%d] \'%s\'' % (cat_cnt, cat_tot, category)
            params = 'key=%s&region=usa&location=%s,%s&radius=%s&format=json&num=50&category=%s' % (api_key, self.lat, self.lon, self.rad, category) #&mappable=address
            url = '%s?%s' % (base_url, params)
            page = 1
            while True:
                if self.debug: print '[!] ' % (url)
                try: response = urllib2.urlopen(url)
                except urllib2.HTTPError as e:
                    if self.debug: print '[!] Error: %s in %s' % (e, url)
                    continue
                jsonstr = response.read()
                if not jsonstr: break
                try: jsonobj = json.loads(jsonstr[13:-2])
                except ValueError as e:
                    if self.debug: print '[!] Error: %s in %s' % (e, url)
                    continue
                if not 'listings' in jsonobj: break
                for item in jsonobj['listings']:
                    dict = {}
                    try:
                        item_category = item['category']['id']
                        dict['source'] = 'Oodle'
                        dict['media_url'] = item['url']
                        dict['thumb_url'] = 'images/oodle_prof.png'
                        dict['lat'] = item['location']['latitude']
                        dict['lon'] = item['location']['longitude']
                        dict['profile_url'] = item['url']
                        dict['display_name'] = self.sanitize(item['source']['name']) #['id']
                        dict['text'] = '%s - %s' % (item_category, self.sanitize(item['title'])) #['body']
                        dict['media_time'] = self.format_time(datetime.fromtimestamp(float(item['ctime'])))
                    except Exception as e:
                        if self.debug: print '[!] Error: %s in %s' % (e, item)
                        continue
                    api_data.append(dict)
                    if self.verbose: print '[+] %s - %s' % (dict['display_name'], dict['text'])
                if jsonobj['meta']['last'] == jsonobj['meta']['total']: break
                page += 1
                if self.verbose: print '[-] Page: %d' % (page)
                url = '%s?%s&start=%d' % (base_url, params, (jsonobj['meta']['last'] + 1))
            cat_cnt += 1
        return api_data

    def get_twitter(self):
        api_data = []
        base_url = 'http://search.twitter.com/search.json'
        params = 'geocode=%s,%s,%skm&rpp=100' % (self.lat, self.lon, self.rad)
        url = '%s?%s' % (base_url, params)
        while True:
            if self.debug: print '[!] ' % (url)
            try: response = urllib2.urlopen(url)
            except urllib2.HTTPError as e:
                if self.debug: print '[!] Error: %s in %s' % (e, url)
                if e.code == 403: break
                continue
            jsonstr = response.read()
            if not jsonstr: break
            try: jsonobj = json.loads(jsonstr)
            except ValueError as e:
                if self.debug: print '[!] Error: %s in %s' % (e, url)
                continue
            for item in jsonobj['results']:
                dict = {}
                if not item['geo']:
                    #if self.verbose: print '[-] Tweet Skipped. No Geo Info.'
                    continue
                try:
                    user_id = item['from_user']
                    tweet_id = item['id']
                    dict['source'] = 'Twitter'
                    dict['media_url'] = 'http://twitter.com/%s/statuses/%s' % (user_id, tweet_id)
                    dict['thumb_url'] = item['profile_image_url']
                    dict['lat'] = item['geo']['coordinates'][0]
                    dict['lon'] =  item['geo']['coordinates'][1]
                    dict['profile_url'] = 'http://twitter.com/%s' % user_id
                    dict['display_name'] = self.sanitize(item['from_user_name'])
                    dict['text'] =  self.sanitize(item['text'])
                    dict['media_time'] = self.format_time(datetime.strptime(item['created_at'], '%a, %d %b %Y %H:%M:%S +0000'))
                except Exception as e:
                    if self.debug: print '[!] Error: %s in %s' % (e, item)
                    continue
                api_data.append(dict)
                if self.verbose: print '[+] %s - %s' % (dict['display_name'], dict['text'])
            if not 'next_page' in jsonobj: break
            if self.verbose: print '[-] Page: %d' % (jsonobj['page'] + 1)
            url = '%s%s' % (base_url, jsonobj['next_page'])
        return api_data

    def get_flickr(self):
        api_data = []
        api_key = self.get_key('flickr')
        if not api_key:
            print '[!] No API Key. Skipping Module.'
            return api_data
        base_url = 'http://api.flickr.com/services/rest/'
        params = 'method=flickr.photos.search&format=json&api_key=%s&text=-&lat=%s&lon=%s&has_geo=1&extras=date_upload,date_taken,owner_name,geo,url_t,url_m&radius=%s&radius_units=km&per_page=100' % (api_key, self.lat, self.lon, self.rad)
        url = '%s?%s&page=41' % (base_url, params)
        jsonstr_last = ''
        while True:
            if self.debug: print '[!] ' % (url)
            try: response = urllib2.urlopen(url)
            except urllib2.HTTPError as e:
                if self.debug: print '[!] Error: %s in %s' % (e, url)
                continue
            jsonstr = response.read()
            # check for same results as last request - broken api
            if jsonstr[100:] == jsonstr_last[100:]: break
            else: jsonstr_last = jsonstr
            if not jsonstr: break
            try: jsonobj = json.loads(jsonstr[14:-1])
            except ValueError as e:
                if self.debug: print '[!] Error: %s in %s' % (e, url)
                continue
            if not 'photos' in jsonobj: break
            #if not len(jsonobj['photos']['photo']) > 0: break
            for item in jsonobj['photos']['photo']:
                dict = {}
                try:
                    user_id = item['owner']
                    dict['source'] = 'Flickr'
                    dict['media_url'] = item['url_m']
                    dict['thumb_url'] = item['url_t']
                    dict['lat'] = item['latitude']
                    dict['lon'] = item['longitude']
                    dict['profile_url'] = 'http://flickr.com/photos/%s' % user_id
                    dict['display_name'] = self.sanitize(item['ownername'])
                    dict['text'] = self.sanitize(item['title'])
                    dict['media_time'] = self.format_time(datetime.strptime(item['datetaken'], '%Y-%m-%d %H:%M:%S'))
                except Exception as e:
                    if self.debug: print '[!] Error: %s in %s' % (e, item)
                    continue
                api_data.append(dict)
                if self.verbose: print '[+] %s - %s' % (dict['display_name'], dict['text'])
            if jsonobj['photos']['page'] == jsonobj['photos']['pages']: break
            if self.verbose: print '[-] Page: %d' % (jsonobj['photos']['page'] + 1)
            url = '%s?%s&page=%s' % (base_url, params, str(jsonobj['photos']['page'] + 1))
        return api_data

    def get_youtube(self):
        api_data = []
        base_url = 'http://gdata.youtube.com/feeds/api/videos'
        params = 'alt=json&location=%s,%s&location-radius=%skm' % (self.lat, self.lon, self.rad) #&max-results=50'
        url = '%s?%s' % (base_url, params)
        page = 1
        while True:
            if self.debug: print '[!] ' % (url)
            try: response = urllib2.urlopen(url)
            except urllib2.HTTPError as e:
                if self.debug: print '[!] Error: %s in %s' % (e, url)
                if e.code == 400: break
                continue
            jsonstr = response.read()
            try: jsonobj = json.loads(jsonstr)
            except ValueError as e:
                if self.debug: print '[!] Error: %s in %s' % (e, url)
                continue
            if jsonobj['feed']['openSearch$totalResults']['$t'] == 0: break
            if not 'entry' in jsonobj['feed']: break
            for item in jsonobj['feed']['entry']:
                dict = {}
                if not item.get('georss$where'):
                    #if self.verbose: print '[-] Video Skipped. No Geo Info.'
                    continue
                try:
                    dict['source'] = 'YouTube'
                    dict['media_url'] = item['link'][0]['href']
                    dict['thumb_url'] = item['media$group']['media$thumbnail'][0]['url']
                    dict['lat'] = item['georss$where']['gml$Point']['gml$pos']['$t'].split()[0]
                    dict['lon'] = item['georss$where']['gml$Point']['gml$pos']['$t'].split()[1]
                    dict['profile_url'] = 'http://www.youtube.com/user/%s' % item['author'][0]['uri']['$t'].split('/')[-1]
                    dict['display_name'] = self.sanitize(item['author'][0]['name']['$t'])
                    dict['text'] = self.sanitize(item['title']['$t'])
                    dict['media_time'] = self.format_time(datetime.strptime(item['published']['$t'], '%Y-%m-%dT%H:%M:%S.%fZ'))
                except Exception as e:
                    if self.debug: print '[!] Error: %s in %s' % (e, item)
                    continue
                api_data.append(dict)
                if self.verbose: print '[+] %s - %s' % (dict['display_name'], dict['text'])
            qty = jsonobj['feed']['openSearch$itemsPerPage']['$t']
            start = jsonobj['feed']['openSearch$startIndex']['$t']
            next = qty + start
            total = jsonobj['feed']['openSearch$totalResults']['$t']
            if next > total: break
            page += 1
            if self.verbose: print '[-] Page: %d' % (page)
            url = '%s?%s&start-index=%s' % (base_url, params, str(next))
        return api_data

    def get_picasa(self):
        import math
        api_data = []
        base_url = "http://picasaweb.google.com/data/feed/api/all"
        kilometers_per_degree_latitude = 111.12
        # http://www.johndcook.com/blog/2009/04/27/converting-miles-to-degrees-longitude-or-latitude
        west_boundary = float(self.lon) - (math.cos(math.radians(float(self.lat))) * float(self.rad) / kilometers_per_degree_latitude)
        south_boundary = float(self.lat) - (float(self.rad) / kilometers_per_degree_latitude)
        east_boundary = float(self.lon) + (math.cos(math.radians(float(self.lat))) * float(self.rad) / kilometers_per_degree_latitude)
        north_boundary = float(self.lat) + (float(self.rad) / kilometers_per_degree_latitude)
        params = "alt=json&bbox=%.6f,%.6f,%.6f,%.6f" % (west_boundary, south_boundary, east_boundary, north_boundary)
        url = '%s?%s' % (base_url, params)
        page = 1
        while True:
            if self.debug: print '[!] ' % (url)
            try: response = urllib2.urlopen(url)
            except urllib2.HTTPError as e:
                if self.debug: print '[!] Error: %s in %s' % (e, url)
                if e.code == 400: break
                continue
            jsonstr = response.read()
            try: jsonobj = json.loads(jsonstr)
            except ValueError as e:
                if self.debug: print '[!] Error: %s in %s' % (e, url)
                continue
            if jsonobj['feed']['openSearch$totalResults']['$t'] == 0: break
            for item in jsonobj['feed']['entry']:
                dict = {}
                if not item.get('georss$where'):
                    #if self.verbose: print '[-] Video Skipped. No Geo Info.'
                    continue
                try:
                    dict['source'] = 'Picasa'
                    dict['media_url'] = item['media$group']['media$content'][0]['url']
                    dict['thumb_url'] = item['media$group']['media$thumbnail'][0]['url']
                    dict['lat'] = item['georss$where']['gml$Point']['gml$pos']['$t'].split()[0]
                    dict['lon'] = item['georss$where']['gml$Point']['gml$pos']['$t'].split()[1]
                    dict['profile_url'] = item['author'][0]['uri']['$t']
                    dict['display_name'] = self.sanitize(item['author'][0]['name']['$t'])
                    dict['text'] = self.sanitize(item['title']['$t'])
                    dict['media_time'] = self.format_time(datetime.strptime(item['published']['$t'], '%Y-%m-%dT%H:%M:%S.%fZ'))
                except Exception as e:
                    if self.debug: print '[!] Error: %s in %s' % (e, item)
                    continue
                api_data.append(dict)
                if self.verbose: print '[+] %s - %s' % (dict['display_name'], dict['text'])
            qty = jsonobj['feed']['openSearch$itemsPerPage']['$t']
            start = jsonobj['feed']['openSearch$startIndex']['$t']
            next = qty + start
            total = jsonobj['feed']['openSearch$totalResults']['$t']
            if next > total: break
            page += 1
            if self.verbose: print '[-] Page: %d' % (page)
            url = '%s?%s&start-index=%s' % (base_url, params, str(next))
        return api_data

    def get_shodan(self):
        api_data = []
        api_key = self.get_key('shodan')
        if not api_key:
            print '[!] No API Key. Skipping Module.'
            return api_data
        base_url = 'http://www.shodanhq.com/api/search'
        params = 'q=geo:%s,%s,%s&key=%s' % (self.lat, self.lon, self.rad, api_key)
        url = '%s?%s' % (base_url, params)
        page = 1
        while True:
            if self.debug: print '[!] ' % (url)
            try: response = urllib2.urlopen(url)
            except urllib2.HTTPError as e:
                if self.debug: print '[!] Error: %s in %s' % (e, url)
                continue
            jsonstr = response.read()
            try: jsonobj = json.loads(jsonstr)
            except ValueError as e:
                if self.debug: print '[!] Error: %s in %s' % (e, url)
                continue
            if not 'matches' in jsonobj: break
            for item in jsonobj['matches']:
                dict = {}
                try:
                    os, hostname = '', ''
                    if 'os' in item: os = item['os']
                    if len(item['hostnames']) > 0: hostname = item['hostnames'][0]
                    protocol = '%s:%d' % (item['ip'], item['port'])
                    dict['source'] = 'Shodan'
                    dict['media_url'] = 'http://www.shodanhq.com/search?q=net:%s' % (item['ip'])
                    dict['thumb_url'] = 'images/shodan_prof.png'
                    dict['lat'] = item['latitude']
                    dict['lon'] = item['longitude']
                    dict['profile_url'] = 'http://%s' % (protocol)
                    dict['display_name'] = protocol
                    dict['text'] =  '%s<br />City: %s, %s<br />OS: %s' % (hostname, item['city'], item['country_name'], os)#, item['data'].strip().replace('\r\n', '<br />'))
                    dict['media_time'] = self.format_time(datetime.strptime(item['updated'], '%d.%m.%Y'))
                except Exception as e:
                    if self.debug: print '[!] Error: %s in %s' % (e, item)
                    continue
                api_data.append(dict)
                if self.verbose: print '[+] %s - %s' % (dict['display_name'], dict['text'])
            break
            page += 1
            if self.verbose: print '[-] Page: %d' % (page)
            url = '%s?%s&page=%s' % (base_url, params, str(page))
        return api_data
    
    
    
#JR Adding function to check Distance
#      
#var R = 6371; // km
#var dLat = (lat2-lat1).toRad();
#var dLon = (lon2-lon1).toRad();
#var lat1 = lat1.toRad();
#var lat2 = lat2.toRad();
#
#var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
#        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
#var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
#var d = R * c;
    def get_distance(self, latitude, longitude):    
        dLat = math.radians(float(self.lat) - latitude)
        dLon = math.radians(float(self.lon) - longitude)
        lat = float(math.radians(latitude))
        lon = float(math.radians(longitude))
        a = math.sin(dLat/2) * math.sin(dLat/2) + math.sin(dLon/2) * math.sin(dLon/2) * math.cos(float(self.lat)) * math.cos(float(latitude)) 
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
        d = 6371 * c
        return d
    


#  JR added section to pare kismet file and 
#  pull the SSID (ESSID, and BSSID) gps info and  
#  other stuff
    def get_kismet(self):
        api_data = []
        #uses lxml to open and parse the xml file
        tree = ET.parse(self.kismet_file, parser=None, base_url=None)
        root = tree.getroot()
        rootElement = root.findall('wireless-network')
        for network in rootElement:
            networktype = network.get('type')
            #don't show probe requests only ad-hoc and infrastructure
            if (networktype != 'probe'):
                lat = network.find('gps-info/avg-lat').text
                lon = network.find('gps-info/avg-lon').text
                dist =  self.get_distance(float(lat), float(lon))
                if (dist <= float(self.rad)):
                    bssid = network.find('BSSID').text
                    essid = network.find('SSID/essid')
                    cloaked = network.get('SSID/essid/[@cloaked]')                
                    timeseen = network.find('seen-card/seen-time')
                    timesplit = timeseen.text.split( )
                    timesplit.pop(0)
                    timejoin = '-'.join(timesplit)
                    security = network.find('SSID/encryption')
                    #do some data clean up
                    if (bssid is None):
                        bssid = 'None'
                    if (essid is None) or (essid.text is None): 
                        essid = 'None'
                    else:
                        essid = essid.text                
                    if (cloaked is None): 
                        cloaked = 'False'  
                    if (security is None):
                        security = 'None'
                    else:
                        security = ''.join(security.text)
                        #JR added to populate from parsing the XML
                    dict = {}
                    dict['source'] = 'kismet'
                    dict['media_url'] = 'http://www.kismetwireless.net'
                    dict['thumb_url'] = 'images/kismet.png'
                    dict['lat'] = lat
                    dict['lon'] = lon
                    dict['profile_url'] = ''
                    dict['display_name'] = networktype +'       '+essid
                    dict['text'] = 'SSID:'+bssid +'- Cloaked:' + cloaked + '- Security:' + security
                    dict['media_time'] = self.format_time(datetime.strptime(timejoin, '%b-%d-%H:%M:%S-%Y'))
                    api_data.append(dict)
                    if self.verbose: print '[+] %s - %s' % (dict['display_name'], dict['text'])
        return api_data


#=================================================
# MAIN FUNCTION
#=================================================

import webbrowser, os

def main():
    import optparse
    usage = '%prog [options]'
    desc = '%prog - Tim Tomes (@LaNMaSteR53) (www.lanmaster53.com)'
    parser = optparse.OptionParser(usage=usage, description=desc)
    parser.add_option('-v', help='enable verbose output', dest='verbose', default=False, action='store_true')
    parser.add_option('-t', metavar='START,STOP', help='date/time range to filter results (ex. 2012-9-5T00:00:00,2012-9-5T23:59:59)', dest='times', type='string', action='store')
    group1 = optparse.OptionGroup(parser, 'Get New Data')
    group1.add_option('-c', metavar='LAT,LON', help='coordinates for center of target area', dest='coords', type='string', action='store')
    group1.add_option('-r', help='radius (km) from given coordinates to collect data', dest='radius', type='string', action='store')
    group1.add_option('-o', help='save data to FILENAME', dest='outfilename', type='string', action='store')
    group1.add_option('--twitter', help='enable Twitter module', dest='twitter', default=False, action='store_true')
    group1.add_option('--youtube', help='enable YouTube module', dest='youtube', default=False, action='store_true')
    group1.add_option('--flickr', help='enable Flickr module', dest='flickr', default=False, action='store_true')
    group1.add_option('--picasa', help='enable Picasa module', dest='picasa', default=False, action='store_true')
    group1.add_option('--instagram', help='enable Instagram module', dest='instagram', default=False, action='store_true')
    group1.add_option('--oodle', help='enable Oodle module', dest='oodle', default=False, action='store_true')
    group1.add_option('--shodan', help='enable Shodan module', dest='shodan', default=False, action='store_true')
    group1.add_option('--all', help='enable all social media modules', dest='all', default=False, action='store_true')
    #JR added kismet option.  Not sure how to remove the =FILENAME thing in the option
    group1.add_option('--kismet', help='enable pulling gps data from kismet file', dest='kismetfile', default=False, type='string', action='store')
    group2 = optparse.OptionGroup(parser, 'Use Existing Data')
    group2.add_option('-i', help='import data from FILENAME', dest='infilename', type='string', action='store')
    parser.add_option_group(group1)
    parser.add_option_group(group2)
    (opts, args) = parser.parse_args()

    if opts.coords and opts.infilename:
        parser.error("[!] Only Provide Coordinates or Input File, not both.")
    elif not opts.coords and not opts.infilename and not opts.kismet:
        parser.error("[!] Must Provide Coordinates or Input File.")
    else:
        if opts.infilename:
            p=pushpin()
            p.verbose = opts.verbose
            p.load_db_from_file(opts.infilename)
        elif opts.coords:
            lat = opts.coords.split(',')[0]
            lon = opts.coords.split(',')[1]
            if opts.radius:
                radius = opts.radius
            else:
                radius = '1'
                print '[-] Using Default Radius of %skm' % radius
            p=pushpin(lat,lon,radius)
            p.verbose = opts.verbose
            #JR added to set the filename for kismet
            if opts.kismetfile:
                p.set_kismet(opts.kismetfile)
            if opts.twitter or opts.youtube or opts.flickr or opts.picasa or opts.shodan or opts.instagram or opts.oodle or opts.kismetfile or opts.all:
                if opts.twitter or opts.all: p.api_to_db('Twitter', 'get_twitter', 'twitter', 'twitter.png', 'images/blue-dot.png')
                if opts.youtube or opts.all: p.api_to_db('YouTube', 'get_youtube', 'youtube', 'youtube.png', 'images/red-dot.png')
                if opts.flickr or opts.all: p.api_to_db('Flickr', 'get_flickr', 'flickr', 'flickr.png', 'images/orange-dot.png')
                if opts.picasa or opts.all: p.api_to_db('Picasa', 'get_picasa', 'picasa', 'picasa.png', 'images/purple-dot.png')
                if opts.instagram or opts.all: p.api_to_db('Instagram', 'get_instagram', 'twitter', 'instagram.png', 'images/blue-dot.png')
                if opts.oodle or opts.all: p.api_to_db('Oodle', 'get_oodle', 'flickr', 'oodle.png', 'images/orange-dot.png')
                if opts.shodan or opts.all: p.api_to_db('Shodan', 'get_shodan', 'youtube', 'shodan.png', 'images/yellow-dot.png')
                #JR added option for kismet file input
                if opts.kismetfile: p.api_to_db('Kismet', 'get_kismet', 'kismet', 'kismet_prof.png')
                if opts.outfilename: p.save_db_to_file(opts.outfilename)
            else:
                parser.error("[!] Must Enable At Least One Module.")

    # exit, do not render pages if no data was found
    has_data = False
    for api in p.db.keys():
        if len(p.db[api]['api_data']) > 0:
            has_data = True
            break
    if not has_data:
        print '[-] No Data Found Within %skm of %s,%s For The Given Modules.' % (p.rad, p.lat, p.lon)
        return

    if opts.times:
        try:
            start = datetime.strptime(opts.times.split(',')[0], '%Y-%m-%dT%H:%M:%S')
            stop = datetime.strptime(opts.times.split(',')[1], '%Y-%m-%dT%H:%M:%S')
            p.time_filter(start, stop)
        except ValueError:
            parser.error("[!] Time Given in Incorrect Format. Correct Format: %s" % (datetime.now().strftime('%Y-%m-%dT%H:%M:%S')))

    content, summary = p.create_media_content()
    p.write_markup('templates/template_media.html', 'pushpin_media.html', content, summary)
    p.write_markup('templates/template_map.html', 'pushpin_map.html', p.create_map_content())

    path = os.getcwd()
    w = webbrowser.get()
    w.open('file://%s/site/pushpin_map.html' % path)
    w.open('file://%s/site/pushpin_media.html' % path)

#=================================================
# START
#=================================================

if __name__ == "__main__": main()
